Apartment.configure do |config|
  config.excluded_models = ["Website"]
  config.database_names = lambda{ Website.scoped.collect(&:domain) }
end
