class CreateWebsites < ActiveRecord::Migration
  def change
    create_table :websites do |t|
      t.string :domain
      t.string :database_name

      t.timestamps
    end
  end
end
